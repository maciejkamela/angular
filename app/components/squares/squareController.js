(function () {
  'use strict';
  var app = angular.module('app');
  app.controller('SquareController', function ($scope) {
    $scope.message = "Hello world";
    $scope.styles = {};
    $scope.squares = [{
      color: 'red'
    }, {
      color: 'blue'
    }, {
      color: 'green'
    }];

    $scope.displayColor = function (square, index) {
      var defaultColor = 'black';
      $scope.selectedSqr = index;
      if ($scope.styles['color'] !== square.color) {
        $scope.styles['color'] = square.color;
      } else {
        $scope.styles['color'] = defaultColor;
      }
      if (square.displayedColor !== square.color) {
        square.displayedColor = square.color;
      } else {
        square.displayedColor = defaultColor;
      }
    };

    $scope.setStyle = function (type, value) {
      $scope.styles[type] = value;
    };

    $scope.resetStyle = function () {
      $scope.styles = {};
      $scope.chosenColor = $scope.defaultColor;
    }
  });
})();
