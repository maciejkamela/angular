module.exports = function (grunt) {
    'use strict';
    let configOptions = {
        'protractor-cucumber-html-report': {
            default_options: {
                options: {
                    dest: 'uat/reports',
                    output: 'report.html',
                    testJSONDirectory: 'uat/reports/json',
                    testJSONResultPath: 'reports/json/report.json'
                }
            }
        }
    };
    grunt.task.registerTask('generateReport', 'protractor-cucumber-html-report');
    let configs = require('load-grunt-configs')(grunt, configOptions);
    require('load-grunt-tasks')(grunt);
    grunt.initConfig(configs);
};
