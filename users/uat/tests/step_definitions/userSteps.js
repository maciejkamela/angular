'use strict';
var
  // world = require('../../config/world.js'),
  pages = require('../../config/pageCollection.js'),
  basePage = pages.BasePage,
  userPage = pages.UsersPage;
var { Given } = require('cucumber');
var { When } = require('cucumber');

const userData = {
  firstName: 'Johny',
  lastName: 'Bravo',
  birthDate: '11-10-1970',
  gender: 'male',
  email: 'johny@super.com',
  mobile: '123456789',
  language: 'English',
  street: 'New York Avenue 14',
  city: 'Cartoon',
  zipCode: '145-478',
  company: 'Bravesco'
};

Given(/^I am on the main user page$/, () => {
  return browser.get('http://protractor-workshop.netlify.com');
});

When(/^I navigate to (contact|home|dashboards|users) tab$/, (tabName) => {
  return basePage.navigateToLocation(tabName)
    .then(() => {
      return browser.sleep(3000);
    });
});

When(/^I add a new user$/, () => {
  return userPage.addUser(userData);
});
