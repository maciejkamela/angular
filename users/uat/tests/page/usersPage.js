'use strict';
let BasePage = require('./basePage'),
  Q = require('q');
function UserPage() {
  const addUserBtn = element(by.css('.add-user')),
    firstNameInput = element(by.model('vm.user.name')),
    lastNameInput = element(by.model('vm.user.surname')),
    birthDateInput = element(by.model('vm.user.dateOb')),
    femaleGender = element(by.css('#female')),
    // maleGender = element(by.css('#male')),
    emailInput = element(by.model('vm.user.email')),
    mobileInput = element(by.model('vm.user.mobile')),
    streetInput = element(by.model('vm.user.street')),
    cityInput = element(by.model('vm.user.city')),
    zipCodeInput = element(by.model('vm.user.zipCode')),
    company = element(by.model('vm.user.company')),
    saveBtn = element(by.buttonText('Save'));

  let selectLanguage = (lang) => {
    console.log(lang);
    return element(by.cssContainingText('option', lang)).click();
  };

  this.addUser = (user) => {
    return addUserBtn.click()
      .then(() => {
        return Q.all([
          firstNameInput.clear().sendKeys(user.firstName),
          lastNameInput.clear().sendKeys(user.lastName),
          birthDateInput.clear().sendKeys(user.birthDate),
          femaleGender.click(),
          emailInput.clear().sendKeys(user.email),
          mobileInput.clear().sendKeys(user.mobile),
          selectLanguage(user.language),
          streetInput.clear().sendKeys(user.street),
          cityInput.clear().sendKeys(user.city),
          zipCodeInput.clear().sendKeys(user.zipCode),
          company.clear().sendKeys(user.company)
        ]);
      })
      .then(() => {
        return saveBtn.click();
      });
  };

}
UserPage.prototype = new BasePage();
module.exports = UserPage;
