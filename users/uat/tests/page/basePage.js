'use strict';

function BasePage() {
  // const contact = element(by.css('.glyphicon-envelope'));

  this.navigateToLocation = (location) => {
    return browser.setLocation(location);
  };

}

module.exports = BasePage;
