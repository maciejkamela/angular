#!/bin/bash

rm exit_codes.txt # this file must be removed only in the first shell of the job, anywhere else!!!
npm install
npm install -g protractor@5.1.1
npm install selenium-webdriver
rm -rf reports
ls -al
cp ./config/test_env.json.example ./config/test_env.json