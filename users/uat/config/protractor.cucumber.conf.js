let path = require('path');

exports.config = {
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
  // seleniumAddress: 'http://localhost:4723/wd/hub',
  jsonReportsDirOutput: '../reports/json',
  downloadsDir: path.dirname(__filename).replace('config', 'tests/downloads'),
  allScriptsTimeout: 90000,
  getPageTimeout: 30000,
  verbose: true,
  specs: ['../tests/features/*.feature'],
  baseUrl: 'http://127.0.0.1:8082/',

  onPrepare: () => {
    browser.manage().window().maximize();
    browser.manage().timeouts().pageLoadTimeout(40000);
    browser.manage().timeouts().implicitlyWait(25000);
    // return browser.get('http://localhost:8080/#/users');
  },
  capabilities: {
    'browserName': 'chrome',
    'chromeOptions': {
      'args': ['--test-type', '--no-sandbox']
    },
    prefs: {
      'download': {
        'prompt_for_download': false,
        'default_directory': path.dirname(__filename).replace('config', 'tests/downloads')//Uncomment this line if you gonna run some downloading tests on your machine
        // 'default_directory': 'downloads'//default download directory for selenium on docker
      }
    }
  },
  cucumberOpts: {
    format: ['pretty'],
    tags: '@dupa',
    require: ['../tests/step_definitions/*.js', './hooks.js']
  },
  rootElement: 'body'
};
