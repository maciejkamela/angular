let path = require('path');

exports.config = {
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  seleniumAddress: 'http://localhost:4723/wd/hub',
  jsonReportsDirOutput: '../reports/json',
  downloadsDir: path.dirname(__filename).replace('config', 'tests/downloads'),
  allScriptsTimeout: 90000,
  getPageTimeout: 30000,
  verbose: true,
  specs: ['../tests/features/*.feature'],
  baseUrl: 'http://protractor-workshop.netlify.com',
  capabilities: {
    'browserName': 'chrome',//for android browser:'Browser' instead of chrome
    platformName: 'android',
    platformVersion: '',
    waitforTimeout: 90000,
    commandTimeout: 90000,
    // deviceName: 'ZY22273Q2K',
    deviceName: 'emulator-5554',
    noReset: true,
    newCommandTimeout: 90000
  },
  cucumberOpts: {
    format: ['pretty'],
    tags: '@demo',
    require: ['../tests/step_definitions/*.js', './hooks.js']
  },
  rootElement: 'body'
};
