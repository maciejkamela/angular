/* jshint node: true */
'use strict';
module.exports = function JsonOutputHook() {
  var Cucumber = require('cucumber'),
    JsonFormatter = Cucumber.Listener.JsonFormatter(),
    fs = require('fs'),
    fsPath = require('path'),
    config = require('./world').config.config,

    deleteFolderRecursive = function(path) {
      if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function(file) {
          var curPath = path + '/' + file;
          if (fs.lstatSync(curPath).isDirectory()) { // recurse
            deleteFolderRecursive(curPath);
          } else { // delete file
            fs.unlinkSync(curPath);
          }
        });
        fs.rmdirSync(path);
      }
    };

  deleteFolderRecursive(config.jsonReportsDirOutput);

  JsonFormatter.log = function(json) {
    var path = fsPath.join(config.jsonReportsDirOutput, 'report.json'),
      directories = config.jsonReportsDirOutput.split('/'),
      currentDir = '';
    console.log(path);
    for (var i = 0; i < directories.length; i++) {
      currentDir += directories[i];
      if (!fs.existsSync(currentDir)) {
        fs.mkdirSync(currentDir);
      }
      currentDir += '/';
    }
    fs.writeFile(path, json, function(err) {
      if (err) {
        throw err;
      }
    });
  };

  this.registerListener(JsonFormatter);
};
