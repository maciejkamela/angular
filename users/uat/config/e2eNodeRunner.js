#!/usr/bin/env node
/* global process */
'use strict';
const nunjucks = require('nunjucks');
const sh = require('shelljs');
const program = require('commander');
const chalk = require('chalk');
const Table = require('cli-table');
const envData = require('../config/test_env.json');
const configTemplate = './config/protractor.cucumber.conf.template.js';
const protractorConfig = './config/protractor.cucumber.conf.js';
const fs = require('fs');
const outputTable = new Table({
  head: ['Tenant', 'Selenium', 'Base url', 'Feature', 'Tags'],
  colWidths: [12, 30, 35, 30, 20]
});

program
  .version('0.0.2')
  .description('For running e2e tests use: npm run e2eTests -- e2eRun with needed option(s).')
  .command('e2eRun')
  .option('-e, --env [name]', 'Use that flag for setting an environment name. The default env is e2edocker.', 'e2edocker')
  .option('-t, --tags <tags>', 'Use that flag for setting a tags. To set more than one tag,' +
    ' list them separated with comma inside quotes eg."@tag1, @tag2".')
  .option('-f, --features <features>', 'Use that flag for setting feature files. For running some certain directory use eg. /administration/**/. ' +
    'The default path is the whole feature directory.', '/')
  .option('-s, --selenium <selenium>', 'Use that flag for setting the selenium address. ' +
    'The default is localhost, for running tests against saucelabs pass "sauce" as a param.', 'http://127.0.0.1:4444/wd/hub')
  .option('-c --config', 'Use that flag exclusively for generating protractor config file. ')
  .action((options) => {
    let envToRun = options.env;
    const context = {
      envToRun: envToRun,
      baseUrl: envData[envToRun].baseUrl,
      seleniumAddress: options.selenium === 'sauce' ? '' : options.selenium,
      tags: options.tags,
      features: options.features,
      seleniumPlatform: process.env.SELENIUM_PLATFORM,
      seleniumBrowserName: process.env.SELENIUM_BROWSER || 'chrome',
      seleniumBrowserVersion: process.env.SELENIUM_VERSION,
      seleniumScreen: process.env.SELENIUM_SCREEN || '1920x1080'
    };
    outputTable.push(
      [context.envToRun, context.seleniumAddress, context.baseUrl, context.features, context.tags || '']
    );
    let renderedConf = nunjucks.render(configTemplate, context);

    fs.writeFile(protractorConfig, renderedConf, function(err) {
      if (err) {
        throw err;
      }
      console.log(chalk.green.bold('Protractor config file has been created with the following data:'));
      console.log(chalk.blue.bold(outputTable.toString()));

      sh.exec('eslint .', function(exitCode) {
        if (exitCode !== 0) {
          console.log(chalk.red.bold('eslint failed, exiting...'));
          process.exitCode = exitCode;
        } else {
          if (!options.config) {
            sh.rm('-rf', './screenShots', './reports');
            sh.exec('protractor ' + protractorConfig, function(code) {
              process.exitCode = code;
            });
          }
        }
      });
    });
  });
program.parse(process.argv);


