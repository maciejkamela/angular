'use strict';
var {defineSupportCode} = require('cucumber');

defineSupportCode(function({setDefaultTimeout}) {
  setDefaultTimeout(10 * 9000);
  console.log('hello from hook');

});
//ToDo dopasuj do nowej wersji cucumbera
// let mkdirp = require('mkdirp'),
//   path = require('path'),
//   fs = require('fs'),
//   Q = require('q');
//
// function writeScreenShot(data, filename) {
//   let failPath = filename.replace('features', '../screenShots'),
//     failDir = path.dirname(failPath);
//   if (!fs.existsSync(failDir)) {
//     mkdirp.sync(failDir);
//   }
//   let stream = fs.createWriteStream(failPath);
//   stream.write(Buffer.from(data, 'base64'));
//   stream.end();
// }
//
// let myHooks = function() {
//   this.setDefaultTimeout(1200000);
//
//   // this.BeforeStep(() => {
//   //   console.log('hello from before step');
//   // });
//   // this.BeforeFeature(() => {
//   //   console.log('hello from before feature');
//   // });
//   // this.FeaturesResult((result) => {
//   //   console.log('duration', result.getDuration());
//   //   console.log('scenarios count', result.getScenarioCounts());
//   //   console.log('steps count', result.getStepCounts());
//   // });
//
//   this.After((scenario) => {
//     return Q.Promise((resolve) => {
//       if (scenario.isFailed()) {
//         return resolve(browser.takeScreenshot()
//           .then((png) => {
//             let fileName = scenario.getUri() + '_ln.' + scenario.getLine() + '.png';
//             writeScreenShot(png, fileName);
//             return png;
//           })
//           .then((png) => {
//             let decodedImage = Buffer.from(png.replace(/^data:image\/(png|gif|jpeg);base64,/, ''), 'base64');
//             scenario.attach(decodedImage, 'image/png');
//           }));
//       } else {
//         return resolve();
//       }
//     });
//   });
// };
//
// module.exports = myHooks;
