# Automatic tests for cumulocity.

***
# Description
Automatic tests written in [Protractor](https://angular.github.io/protractor/#/tutorial).
For this tests you should have a basic background in [cucumber](https://cucumber.io/) and the Gherkin language,
as well as a basic understanding of Protractor and its locators, and also about the page object pattern. 

# Installation

1.  Install NodeJS and NPM

    Firstly  install nodejs

    Ubuntu
    http://richardhsu.net/2013/10/19/installing-nodejs-npm-on-ubuntu-13-10/

    Mac
    http://coolestguidesontheplanet.com/installing-node-js-on-osx-10-10-yosemite/

    Windows
    http://nodejs.org/

2.  Install webdriver-manager (https://www.npmjs.com/package/webdriver-manager)

       npm install -g webdriver-manager
       webdriver-manager update

3. Install protractor globally
    
       npm install protractor -g 
    
## Running tests locally

First you need to start the selenium webdriver manager. Therefore just do
    
    webdriver-manager start
   
Then just run

    protractor path/to/protractor.cucumber.conf.js
    
Then just run
  
      npm run e2eTests -- e2eRun -h
         
  to see a list of available commands. Usually you want to pass -e for the environment and -t for some @tags.
  An usual command during development is therefore something like
          
      npm run e2eTests -- e2eRun -e yourenvironment -t @yourtestingtag
    
  You can also pass an option to limit the feature files processed (without the .feature-suffix)
     
      npm run e2eTests -- e2eRun -e yourenvironment -f /featureFileNameWithoutSuffix
      example npm run e2eTests -- e2eRun -e local -t @demo

### I. Appium
Na Linux trzeba pobrac android_sdk_for_linux, i w tym katalogu odpala sie SDK managera ( w katalogu tools: ./android)
 https://nishantverma.gitbooks.io/appium-for-android/content/starting_appium_server.html
 http://appium.io/slate/en/master/?ruby#setup-android
 ##### 1. Export paths
 ```
 export ANDROID_HOME=$HOME/Android/Sdk
 export PATH=$PATH:$ANDROID_HOME/tools
 
 ```
 ##### 2. Add the following paths to ~/.bash_profile (if there is not such file just create it)
 ```
 #/bin/bash
 
 #Android SDK export paths
 
 export ANDROID_HOME=$HOME/Android/Sdk
 export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
 
 #JAVA JDK PATH
 JAVA_HOME=/usr/lib/jvm/default-java
 export JAVA_HOME
 export PATH=$PATH:$JAVA_HOME/bin
```
 ##### 3. ADD to ~/.bashrc
 ```
 #/bin/bash
 
 #Android SDK export paths
 
 export ANDROID_HOME=$HOME/Android/Sdk
 export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
 
 #JAVA JDK PATH
 JAVA_HOME=/usr/lib/jvm/default-java
 export JAVA_HOME
 export PATH=$PATH:$JAVA_HOME/bin
 ```
 And then source ~/.bashrc and reboot PC
 
 #### 4. run android list sdk
 cd android-for-linux/tools
 i tam ./android sdk -> pobierze obrazy
 jes;li pojawi sie problem z uprawnienami: access denied
 to sudo chmod -R 777 android-for-linux
  
 #### Install appium-doctor
 npm install appium-doctor -g
 
 And then ru:
 appium-doctor
 
 android list sdk --use-sdk-wrapper
 
 #### 5. Polecenia
 ``` adb devices ``` sprawdza podlaczone urzadzenia fizyczne
 ``` cd android-sdk-linux/tools, i tam ./android ``` uruchamia SDK Managera
 #### 6. Droid screen (http://droid-at-screen.org/installation.html)
 Pobierz plik z http://droid-at-screen.org/download.html
 Plik dodoale sobie do config tak aby pamietac
 Trzeba podlaczyc urzadzenie, zobaczyc ze jest na liscie devicow: adb devices 
 i poleceniem java -jar droidAtScreen-a.b.c.jar odpalic 