angular
  .module('app', ['ui.bootstrap', 'leaflet-directive', 'ngMessages', 'ngRoute',
    'mainMenu', 'users', 'home', 'contact', 'userDetails', 'addUserModule', 'docsModule', 'ui.router', 'registrationModule']);
