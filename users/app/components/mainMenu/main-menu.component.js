(function() {
  angular
    .module('mainMenu')
    .component('appMenu', {
      templateUrl: 'app/components/mainMenu/main-menu.template.html',
      controller: MainMenuController,
      controllerAs: 'vm'
    });
  MainMenuController.$inject = ['$location'];
  function MainMenuController($location) {
    var vm = this;
    vm.menu = 'Welcome to main menu';
    vm.isActive = false;
    vm.isRegistrationShown = false;
    vm.cars = ['BMW', 'VW', 'Fiat', 'Ford'];
    vm.menuItems = [
      {
        menuName: 'Home',
        url: 'home',
        icon: 'glyphicon glyphicon-home nav-icon'

      },
      {
        menuName: 'Dashboards',
        url: 'dashboards',
        icon: 'glyphicon glyphicon-dashboard nav-icon'

      },
      {
        menuName: 'Users',
        url: 'users',
        icon: 'glyphicon glyphicon-user nav-icon'
      },
      {
        menuName: 'Contact',
        url: 'contact',
        icon: 'glyphicon glyphicon-envelope nav-icon'
      },
      {
        menuName: 'Docs',
        url: 'docs',
        icon: 'glyphicon glyphicon-book nav-icon'
      }
    ];

    vm.setActive = function(menuItem) {
      vm.activeMenu = menuItem;
    };

    vm.$onInit = function() {
      var activeMenuItem = vm.menuItems.find(function(item) {
        return item.url.indexOf($location.url()) !== -1;
      });
      vm.setActive(activeMenuItem || vm.menuItems[2]);
    };

    vm.showRegistrationForm = function(isShown) {
      console.log(vm.isRegistrationShown, 'is shown');
      vm.isRegistrationShown = isShown;
    };

    vm.test = function(car) {
      console.log('hello from my callback method', car);
    };

  }
})();