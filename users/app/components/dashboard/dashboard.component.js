(function() {
  'use strict';
  angular
    .module('app')
    .component('appDashboard', {
      bindings: {
        message: '@',// toDo zobacz jak zrobic zeby ten komponent mogl byc uzyty gdzies indziej
        property:'<'
      },
      templateUrl: 'app/components/dashboard/dashboard.template.html',
      controller: DashboardsController,
      controllerAs: 'vm'
    });

  function DashboardsController() {
    var vm = this;
    vm.message = 'jakis text';
  }
})();