(function() {
  'use strict';
  angular
    .module('home')
    .component('appHome', {
      templateUrl: 'app/components/home/home.template.html',
      controller: HomeController,
      controllerAs: 'vm'
    });

  function HomeController() {
    var vm = this;
    vm.message = 'Welcome to my home';
  }
})();