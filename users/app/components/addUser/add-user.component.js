(function() {
  'use strict';
  angular
    .module('addUserModule')
    .component('appAddUser', {
      templateUrl: 'app/components/addUser/add-user.template.html',
      controller: AddUserController,
      controllerAs: 'vm'
    });
  AddUserController.$inject = ['userService', '$timeout', '$location'];

  function AddUserController(userService, $timeout, $location) {
    var vm = this;
    vm.isUserAdded = false;
    vm.female = 'female';
    vm.male = 'male';
    vm.currentLang = 'Polish';
    vm.openedCalendar = false;
    vm.format = 'dd-MMMM-yyyy';
    vm.pattern = '/^[0-9]{2}\-[0-9]{2}\-[0-9]{4}$/i';
    vm.dateOptions = {
      formatYear: 'yy',
      maxDate: new Date(2020, 5, 22),
      minDate: new Date(1900, 5, 22),
      startingDay: 1
    };

    vm.languages = [
      'Polish',
      'English',
      'German',
      'Russian',
      'Other'
    ];
    vm.user = {
      id: Math.floor((Math.random() * 200) + 1),
      name: '',
      surname: '',
      dateOb: new Date(1950, 9, 22),
      gender: '',
      email: '',
      mobile: '',
      language: 'Polish',
      street: '',
      city: '',
      zipCode: '',
      company: '',
      address: '',
      fullName: ''
    };
    vm.openCalendar = function() {
      return vm.openedCalendar = !vm.openedCalendar;
    };

    vm.hideAlert = function() {
      $timeout(function() {
        vm.isUserAdded = !vm.isUserAdded;
        return $location.url('/users');
      }, 3000);
    };

    vm.saveUser = function() {
      vm.user.address = vm.user.city + ' ' + vm.user.street;
      vm.user.fullName = vm.user.name + ' ' + vm.user.surname;
      console.log(vm.user);
      vm.isUserAdded = !vm.isUserAdded;
      vm.hideAlert();
      userService.addUser(vm.user);
    };

    vm.goToUsers = function() {
      // return $location.url('users');
      return window.history.back();
    }
  }
})();