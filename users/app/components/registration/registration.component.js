(function() {
  'use strict';
  angular
    .module('registrationModule')
    .component('appRegistration', {
      templateUrl: 'app/components/registration/registration.template.html',
      controller: RegistrationController,
      controllerAs: 'vm',
      bindings: {
        name: '@',
        cars: '<',
        onDelete: '&',
        onToggle: '&'
      }
    });
  // RegistrationController.$inject = ['userService', '$timeout', '$location'];

  function RegistrationController() {
    var vm = this;
    vm.updateProp = function() {
      // vm.parentProp = 'jakis tam';
      console.log(vm.cars, 'parent prop');
    };

    vm.delete = function() {
      vm.onDelete({car: vm.cars[0]})// nazwa klucza (car) musi byc taka sama jak w parent html-u
    };

    vm.hideModal = function() {
      vm.onToggle({isRegistrationVisible: false});
    }

  }
})();