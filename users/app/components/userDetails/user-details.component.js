(function() {
  'use strict';
  angular
    .module('userDetails')
    .component('appUserDetails', {
      bindings: {
        testCamel: '<'// toDo zobacz jak zrobic zeby ten komponent mogl byc uzyty gdzies indziej
      },
      templateUrl: 'app/components/userDetails/user-details.template.html',
      controller: UserDetailsController,
      controllerAs: 'vm'
    });
  UserDetailsController.$inject = ['$stateParams', 'userService'];

  function UserDetailsController($stateParams, userService, $scope) {
    var vm = this;
    vm.example = {
      value: new Date(2013, 9, 22)
    };
    vm.testMsg = 'some sample text';
    vm.currentUser = {};
    vm.isDisabled = true;
    vm.loadMore = false;
    vm.$onInit = function() {
      console.log($stateParams, 'route');
      vm.userId = $stateParams.userId;
      return getUser(vm.userId);
    };

    function getUser(userId) {
      console.log(userId, 'user id adadada');
      return userService.getUser(userId)
        .then(function(user) {
          console.log(user, 'user jjjjjjj');
          vm.currentUser = user;
          vm.currentUserdateOb = new Date(user.dateOb);
          return user;
        })
    }

    vm.enableUserForm = function(isValid) {
      if (isValid) {
        vm.isDisabled = !vm.isDisabled;
        //edit and then save a form
      } else {
        console.log('form is invalid');
      }
    };

    vm.loadMoreUserDetails = function() {
      vm.loadMore = !vm.loadMore;
    };

    vm.saveUser = function(newUserData) {
      return userService.updateUser(newUserData);
    };


    // $scope.$watch('vm.currentUser', function (newValue) {
    //   vm.currentUser.dateOb = $filter('date')(newValue, 'yyyy/MM/dd');
    // });
    //
    // $scope.$watch('vm.currentUser.dateOb', function (newValue) {
    //   vm.currentUser.dateOb = $filter('date')(newValue, 'yyyy/MM/dd');
    // });
  }
})();