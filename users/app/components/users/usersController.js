(function() {
  'use strict';
  angular
    .module('app')
    .controller('UsersController', UserController);

  UserController.$inject = ['userService'];

  function UserController(userService) {

    var vm = this;
    vm.users = [];
    vm.editingUsers = {};
    vm.checkedUsersIndexes = [];
    vm.sortedCols = ['id'];
    vm.sortReverse = false;
    vm.checkedUsersCollection = [];
    vm.toggleView = false;

    getUsers();

    function getUsers() {
      return userService.getUsers()
        .then(function(data) {
          vm.users = data;
          return vm.users;
        });
    }

    vm.deleteUser = function(user) {
      //zobacz funckje map albo filter tutaj https://www.sitepoint.com/es6-arrow-functions-new-fat-concise-syntax-javascript/
      angular.forEach(vm.users, function(item, i) {
        if (item.id === user.id) {
          vm.users.splice(i, 1);
        }
      });
    };

    vm.editUser = function(user) {
      vm.editingUsers[user.id] = true;
    };

    vm.updateUser = function(user) {
      vm.editingUsers[user.id] = false;
    };

    vm.countUsers = function() {
      return vm.users.length;
    };
    vm.setWarningLevel = function() {
      return vm.countUsers() < 20 ? "label-warning" : "label-success";
    };

    vm.sortBy = function(headColName) {
      vm.sortReverse = (vm.sortReverse === false);
      vm.sortedCols = [];
      return vm.sortedCols.push(headColName);
    };

    vm.checked = function(user) {
      vm.checkedUsersCollection.push(user.id);
    };

    vm.resetChecked = function() {
      angular.forEach(vm.users, function(user) {
        user.isChecked = false;
      });
    };

    vm.editUsersCollection = function() {
      angular.forEach(vm.checkedUsersCollection, function(id) {
        vm.editingUsers[id] = !vm.editingUsers[id];
      });
      vm.resetChecked();
      vm.toggleView = !vm.toggleView;
    };
    vm.alert = function(user) {
      alert(user.name + ' ' + user.surname);
    };
  }
})();
