/**
 * Created by maciejkamela on 25.10.16.
 */
angular
  .module('app')
  .directive('usersDirective', function () {
    return {
      restrict: 'EA',
      scope: true,
      // controllerAs: 'UsersController as vm',
      templateUrl: 'app/components/users/users.template.html'
    };
  });