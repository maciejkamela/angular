angular
.module('app')
.directive('displayUsers', function () {
  return {
    restrict: 'AE',
    replace:false,
    scope: {},
    templateUrl: 'app/components/users/users.template.html'
  };
});