(function() {
  'use strict';
  angular
    .module('users')
    .component('appUsers', {
      templateUrl: 'app/components/users/users.template.html',
      controller: UserController,
      controllerAs: 'vm'
    });
  UserController.$inject = ['userService'];

  function UserController(userService, $scope) {

    var vm = this;
    vm.users = [];
    vm.editingUsers = {};
    vm.checkedUsersIndexes = [];
    vm.sortedCols = ['id'];
    vm.sortReverse = false;
    vm.checkedUsersCollection = [];
    vm.toggleView = false;
    vm.isAddUserFormShown = false;
    vm.allUsers = [];
    vm.currentPage = 1;
    vm.searchUser = '';
    vm.showClearSearchUserIcon = false;

    vm.$onInit = function() {
      vm.loadUsersPerPage();
      loadAllUsers();
    };

    vm.loadUsersPerPage = function() {
      return userService.getUsers(vm.currentPage)
        .then(function(data) {
          // debugger;
          vm.users = data;
          return vm.users;
        });
    };

    function loadAllUsers() {
      return userService.getAllUsers()
        .then(function(data) {
          vm.allUsers = data;
          return vm.allUsers;
        });
    }

    vm.deleteUser = function(userId) {
      return userService.deleteUser(userId)
        .then(function() {
          return vm.countUsers();
        })
        .then(function() {
          return loadAllUsers();
        })
        .then(function() {
          return vm.loadUsersPerPage();
        });
    };

    vm.editUser = function(user) {
      vm.editingUsers[user.id] = true;
    };

    vm.updateUser = function(user) {
      console.log('update user', user);
      vm.editingUsers[user.id] = false;
      return userService.updateUser(user);
    };

    vm.countUsers = function() {
      return vm.allUsers.length;
    };
    vm.setWarningLevel = function() {
      return vm.countUsers() < 20 ? "label-warning" : "label-success";
    };

    vm.sortBy = function(headColName) {
      vm.sortReverse = (vm.sortReverse === false);
      vm.sortedCols = [];
      return vm.sortedCols.push(headColName);
    };

    vm.checked = function(user) {
      vm.checkedUsersCollection.push(user.id);
    };

    vm.resetChecked = function() {
      angular.forEach(vm.users, function(user) {
        user.isChecked = false;
      });
    };

    vm.editUsersCollection = function() {
      angular.forEach(vm.checkedUsersCollection, function(id) {
        vm.editingUsers[id] = !vm.editingUsers[id];
      });
      vm.resetChecked();
      vm.toggleView = !vm.toggleView;
    };

    vm.showAddUserForm = function() {
      console.log(vm.isAddUserFormShown, '******');
      return vm.isAddUserFormShown = !vm.isAddUserFormShown;
    };

    vm.findUser = function(query) {
      console.log(query, 'query');
      return userService.findUser(query)
        .then(function(data) {
          vm.users = data;
          return vm.users;
        });
    };
    vm.restoreUserList = function() {
      vm.searchUser = '';
      vm.loadUsersPerPage();
    };
    // $scope.currentPage = 1;
    // $scope.$watch('currentPage', function() {
    //   alert('hey, page has changed');
    // });
    //

  }


})();