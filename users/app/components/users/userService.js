angular
  .module('app')
  .factory('userService', userService);

userService.$inject = ['$http', '$location'];

function userService($http, $location) {
  return {
    getUsers: getUsers,
    getUser: getUser,
    getAllUsers: getAllUsers,
    deleteUser: deleteUser,
    updateUser: updateUser,
    findUser: findUser,
    getLocation: getLocation,
    addUser: addUser
    // ,
    // getUsersFromArray: getUsersFromArray
  };


  function getUsersComplete(response) {
    return response.data;
  }

  function getUsersFailed(error) {
    console.error('XHR Failed for getUsers.' + error.data);
  }

  function getUsers(pageNo) {
    return $http({
      url: 'http://localhost:3000/users?_page=' + pageNo + '&_limit=7',
      method: 'GET'
    })
      .then(getUsersComplete)
      .catch(getUsersFailed);
  }

  function getAllUsers() {
    return $http({
      url: 'http://localhost:3000/users',
      method: 'GET'
    })
      .then(getUsersComplete)
      .catch(getUsersFailed);
  }

  function getUser(userId) {
    return $http.get('http://localhost:3000/users/' + userId)
      .then(getUsersComplete)
      .catch(getUsersFailed);
  }

  function findUser(query) {
    return $http.get('http://localhost:3000/users/?q=' + query)
      .then(getUsersComplete)
      .catch(getUsersFailed);
  }

  function deleteUser(userId) {
    return $http.delete('http://localhost:3000/users/' + userId);
  }

  function updateUser(data) {
    return $http.put('http://localhost:3000/users/' + data.id, data)
      .then(getUsersComplete)
      .catch(getUsersFailed);
  }

  function addUser(data) {
    return $http.post('http://localhost:3000/users/', data)
      .then(getUsersComplete)
      .catch(getUsersFailed);
  }

  function getLocation() {
    return $location.path();
  }

  // function getUsersFromArray() {
  //   return $http.get('app/resources/users.json')
  //     .then(getUsersComplete)
  //     .catch(getUsersFailed);
  // }

}
