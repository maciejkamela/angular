(function() {
  'use strict';
  angular
    .module('contact')
    .component('appContact', {
      templateUrl: 'app/components/contact/contact.template.html',
      controller: ContactController,
      controllerAs: 'vm'
    });

  function ContactController($scope, $timeout) {
    var vm = this;
    vm.contact = 'My personal contact data:';
    vm.isDisabled = true;
    vm.isSent = false;
    vm.isFormOpen = false;
    vm.contactMessage = {
      name: "",
      phone: "",
      email: "",
      msg: ""
    };
    vm.isSubmitted = false;

    vm.hideAlert = function() {
      $timeout(function() {
        return vm.isSent = !vm.isSent;
      }, 4000);
    };
    vm.contactData = [
      {
        name: 'Address',
        value: 'Poznan, ul. Jakas 78'
      },
      {
        name: 'Phone',
        value: '+48 123 456 789'
      },
      {
        name: 'Email',
        value: 'some@example.gmail.com'
      }
    ];
    vm.submitMsg = function(isValid) {
      vm.isSubmitted = true;
      if (isValid) {
        vm.isDisabled = !vm.isDisabled;
        vm.isSent = !vm.isSent;
        vm.contactMessage = {};
        vm.isFormOpen = !vm.isFormOpen;
      }
    };

    var mainMarker = {
      lat: 52.4064,
      lng: 16.9252,
      focus: true,
      message: "Poznan, Headquarter",
      draggable: true
    };

    vm.poznan = {
      lat: 52.4064,
      lng: 16.9252,
      zoom: 17
    };

    vm.markers = {
      mainMarker: angular.copy(mainMarker)
    };

    vm.position = {
      lat: 52.4064,
      lng: 16.9252
    };
    vm.events = { // or just {} //all events
      markers: {
        enable: ['dragend'],
        logic: 'emit'
      }
    };

    $scope.$on("leafletDirectiveMarker.dragend", function(event, args) {
      $scope.vm.position.lat = args.model.lat;
      $scope.vm.position.lng = args.model.lng;
    });

  }
})();