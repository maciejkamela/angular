angular
  .module('app')
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/users');
    $stateProvider
      .state('home', {
        url: '/home',
        template: '<app-home>'
      })
      .state('dashboards', {
        url: '/dashboards',
        template: '<app-dashboard>'
      })
      .state('contact', {
        url: '/contact',
        template: '<app-contact>'
      })
      .state('users', {
        url: '/users',
        template: '<app-users>'
      })
      .state('newuser', {
        url: '/users/new',
        template: '<app-add-user>'
      })
      .state('docs', {
        url: '/docs',
        template: '<app-docs>'
      })
      .state('user', {
        url: '/users/{userId}',
        template: '<app-user-details>'
      })
      .state('register', {
        url: '/register',
        template: '<app-registration>'
      });
  });


// angular
//   .module('app')
//   .config(['$locationProvider', '$routeProvider',
//     function($locationProvider, $routeProvider) {
//       // $locationProvider.hashPrefix('!');
//       $routeProvider
//         .when('/home', {
//           template: '<app-home>'
//         })
//         .when('/dashboards', {
//           template: '<app-dashboard>'
//         })
//         .when('/contact', {
//           template: '<app-contact>'
//         })
//         .when('/users', {
//           template: '<app-users>'
//         })
//         .when('/users/new', {
//           template: '<app-add-user>'
//         })
//         .when('/users/:userId', {
//           template: '<app-user-details>'
//         })
//         .when('/docs', {
//           template: '<app-docs>'
//         })
//         .otherwise('/users');
//     }
//   ]);