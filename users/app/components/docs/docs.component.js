(function() {
  angular
    .module('docsModule')
    .component('appDocs', {
      templateUrl: 'app/components/docs/docs.template.html',
      controller: DocsController,
      controllerAs: 'vm'
    });
  // DocsController.$inject = ['$location'];
  function DocsController() {
    var vm = this;
    vm.menu = 'Welcome to main menu';
    vm.tools = [
      {
        name: 'Git',
        url: 'https://git-scm.com/book/en/v2/Getting-Started-Installing-Git',
        icon: 'app/resources/git.png',
        iconColor: '#F34C28',
        description: 'Is a version control system (VCS) for tracking changes in computer files and coordinating work on' +
        'those files among multiple people. It is primarily used for software development, but it can be used to keep track changes in any files.'
      },
      {
        name: 'NodeJS',
        url: 'https://nodejs.org/en/download/',
        icon: 'app/resources/nodejs.png',
        iconColor: '#4A8C45',
        description: 'As an asynchronous event driven JavaScript runtime, Node is designed to build scalable network applications.'
      },
      {
        name: 'Protractor',
        url: 'http://www.protractortest.org/#/protractor-setup',
        icon: 'app/resources/protractor.png',
        iconColor: '',
        description: 'Protractor is an end-to-end test framework for Angular and AngularJS applications. ' +
        'Protractor runs tests against your application running in a real browser, interacting with it as a user would.'
      },
      {
        name: 'Selenium server',
        url: 'http://www.protractortest.org/#/server-setup',
        icon: 'app/resources/selenium.png',
        iconColor: '',
        description: 'When working with Protractor, you need to specify how to connect to the browser drivers which will start up and control the browsers you are testing on. ' + '' +
        'You will most likely use the Selenium Server. The server acts as proxy between your test script (written with the WebDriver API) and the browser driver (controlled by the WebDriver protocols).'
      },
      {
        name: 'Cucumber',
        url: 'https://cucumber.io/docs/reference/javascript',
        icon: 'app/resources/cucumber.png',
        iconColor: '',
        description: 'Cucumber merges specification and test documentation into one cohesive whole. ' +
        'Cucumber supports over a dozen different software platforms. Every Cucumber implementation provides the same overall ' +
        'functionality, but they also have their own installation procedure and platform-specific functionality.'
      },
      {
        name: 'Gherkin',
        url: 'https://cucumber.io/docs/reference',
        icon: 'app/resources/gherkin.jpg',
        iconColor: '',
        description: 'Gherkin is plain-text English (or one of 60+ other languages) with a little extra structure. ' +
        'Gherkin is designed to be easy to learn by non-programmers, yet structured enough to allow concise description of examples ' +
        'to illustrate business rules in most real-world domains.'
      },
      {
        name: 'Chai assertion library',
        url: 'http://chaijs.com/guide/installation/',
        icon: 'app/resources/chai.png',
        iconColor: '',
        description: 'Chai is a BDD / TDD assertion library for node and the browser that can be delightfully paired with any javascript testing framework.' +
        'Chai has several interfaces that allow the developer to choose the most comfortable. The chain-capable BDD styles provide an expressive language & readable style, while the TDD assert style provides a more classical feel.'
      }
    ];
  }
})();