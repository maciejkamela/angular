module.exports = {
  context: __dirname + '/app',
  entry: '../app.js',
  output: {
    path: __dirname + '/app',
    filename: '../bundle.js'
  },
  devtool: 'sourcemap',
  module: {
    loaders: [
      {tests: /\.html$/, loader: 'raw'},
      {tests: /\.styl$/, loader: 'style!css!stylus'}
    ]
  }
};